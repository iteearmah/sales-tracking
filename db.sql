-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 23, 2016 at 07:35 PM
-- Server version: 5.7.15-0ubuntu0.16.04.1
-- PHP Version: 7.0.8-0ubuntu0.16.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `barbershop`
--

-- --------------------------------------------------------

--
-- Table structure for table `aauth_groups`
--

CREATE TABLE `aauth_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `definition` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aauth_groups`
--

INSERT INTO `aauth_groups` (`id`, `name`, `definition`) VALUES
(1, 'Admin', 'Super Admin Group'),
(3, 'Sales', 'Sales Access Group');

-- --------------------------------------------------------

--
-- Table structure for table `aauth_group_to_group`
--

CREATE TABLE `aauth_group_to_group` (
  `group_id` int(11) UNSIGNED NOT NULL,
  `subgroup_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_login_attempts`
--

CREATE TABLE `aauth_login_attempts` (
  `id` int(11) NOT NULL,
  `ip_address` varchar(39) DEFAULT '0',
  `timestamp` datetime DEFAULT NULL,
  `login_attempts` tinyint(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aauth_login_attempts`
--

INSERT INTO `aauth_login_attempts` (`id`, `ip_address`, `timestamp`, `login_attempts`) VALUES
(1, '127.0.0.1', '2016-07-30 23:22:55', 1),
(2, '127.0.0.1', '2016-07-31 00:38:22', 9),
(3, '127.0.0.1', '2016-07-31 00:50:34', 6);

-- --------------------------------------------------------

--
-- Table structure for table `aauth_perms`
--

CREATE TABLE `aauth_perms` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `definition` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aauth_perms`
--

INSERT INTO `aauth_perms` (`id`, `name`, `definition`) VALUES
(1, 'access_sales_reports', 'Access Sales Area'),
(2, 'access_users', 'Access Users Area');

-- --------------------------------------------------------

--
-- Table structure for table `aauth_perm_to_group`
--

CREATE TABLE `aauth_perm_to_group` (
  `perm_id` int(11) UNSIGNED NOT NULL,
  `group_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aauth_perm_to_group`
--

INSERT INTO `aauth_perm_to_group` (`perm_id`, `group_id`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `aauth_perm_to_user`
--

CREATE TABLE `aauth_perm_to_user` (
  `perm_id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aauth_perm_to_user`
--

INSERT INTO `aauth_perm_to_user` (`perm_id`, `user_id`) VALUES
(1, 6),
(2, 6);

-- --------------------------------------------------------

--
-- Table structure for table `aauth_pms`
--

CREATE TABLE `aauth_pms` (
  `id` int(11) UNSIGNED NOT NULL,
  `sender_id` int(11) UNSIGNED NOT NULL,
  `receiver_id` int(11) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `message` text,
  `date_sent` datetime DEFAULT NULL,
  `date_read` datetime DEFAULT NULL,
  `pm_deleted_sender` int(1) DEFAULT NULL,
  `pm_deleted_receiver` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aauth_users`
--

CREATE TABLE `aauth_users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(100) NOT NULL,
  `pass` varchar(64) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `banned` tinyint(1) DEFAULT '0',
  `last_login` datetime DEFAULT NULL,
  `last_activity` datetime DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `forgot_exp` text,
  `remember_time` datetime DEFAULT NULL,
  `remember_exp` text,
  `verification_code` text,
  `totp_secret` varchar(16) DEFAULT NULL,
  `ip_address` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aauth_users`
--

INSERT INTO `aauth_users` (`id`, `email`, `pass`, `username`, `banned`, `last_login`, `last_activity`, `date_created`, `forgot_exp`, `remember_time`, `remember_exp`, `verification_code`, `totp_secret`, `ip_address`) VALUES
(1, 'admin@example.com', 'dd5073c93fb477a167fd69072e95455834acd93df8fed41a2c468c45b394bfe3', 'Admin_', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0'),
(2, 'lilmopat@gmail.com', '85331630fca2b67c234b6b57e7affc9403d62cf186989c71675956e3ccc2a20d', 'admin', 0, '2016-10-11 19:12:16', '2016-10-11 19:12:16', '2016-07-30 23:36:33', NULL, '2016-08-03 00:00:00', 'pqWuhkrUsvy9zJCS', NULL, NULL, '127.0.0.1'),
(7, 'iteearmah@gmail.com', 'e2b0b50799604ff03bb79e53c7e1f716fb4a0521d4fc39c2af997f6385adcd96', 'iteearmah', 0, '2016-10-11 17:16:06', '2016-10-11 17:16:06', '2016-08-14 11:30:17', NULL, NULL, NULL, NULL, NULL, '127.0.0.1');

-- --------------------------------------------------------

--
-- Table structure for table `aauth_user_to_group`
--

CREATE TABLE `aauth_user_to_group` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aauth_user_to_group`
--

INSERT INTO `aauth_user_to_group` (`user_id`, `group_id`) VALUES
(1, 1),
(1, 3),
(2, 1),
(3, 3),
(4, 3),
(5, 3),
(6, 1),
(7, 3);

-- --------------------------------------------------------

--
-- Table structure for table `aauth_user_variables`
--

CREATE TABLE `aauth_user_variables` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `data_key` varchar(100) NOT NULL,
  `value` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `audit_trail`
--

CREATE TABLE `audit_trail` (
  `id` int(11) NOT NULL,
  `section` varchar(100) NOT NULL,
  `action` text NOT NULL,
  `action_by` int(11) NOT NULL,
  `action_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('e4887f830f4f371cbbcaaf39ad15f4cc9efd785c', '127.0.0.1', 1476206166, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437363230363136363b),
('49eeef4429beb98acd8cf33fa7291d169fbe1406', '127.0.0.1', 1476206479, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437363230363437393b69647c733a313a2237223b757365726e616d657c733a393a226974656561726d6168223b656d61696c7c733a31393a226974656561726d616840676d61696c2e636f6d223b6c6f67676564696e7c623a313b75736572696e666f7c4f3a383a22737464436c617373223a31343a7b733a323a226964223b733a313a2237223b733a353a22656d61696c223b733a31393a226974656561726d616840676d61696c2e636f6d223b733a343a2270617373223b733a36343a2265326230623530373939363034666630336262373965353363376531663731366662346130353231643466633339633261663939376636333835616463643936223b733a383a22757365726e616d65223b733a393a226974656561726d6168223b733a363a2262616e6e6564223b733a313a2230223b733a31303a226c6173745f6c6f67696e223b733a31393a22323031362d31302d31312031373a31363a3036223b733a31333a226c6173745f6163746976697479223b733a31393a22323031362d31302d31312031373a31363a3036223b733a31323a22646174655f63726561746564223b733a31393a22323031362d30382d31342031313a33303a3137223b733a31303a22666f72676f745f657870223b4e3b733a31333a2272656d656d6265725f74696d65223b4e3b733a31323a2272656d656d6265725f657870223b4e3b733a31373a22766572696669636174696f6e5f636f6465223b4e3b733a31313a22746f74705f736563726574223b4e3b733a31303a2269705f61646472657373223b733a393a223132372e302e302e31223b7d),
('afc0fdc45f6212dbb350b5967d872102abfda6f6', '127.0.0.1', 1476206812, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437363230363831323b69647c733a313a2232223b757365726e616d657c733a363a22626172626572223b656d61696c7c733a31383a226c696c6d6f70617440676d61696c2e636f6d223b6c6f67676564696e7c623a313b75736572696e666f7c4f3a383a22737464436c617373223a31343a7b733a323a226964223b733a313a2232223b733a353a22656d61696c223b733a31383a226c696c6d6f70617440676d61696c2e636f6d223b733a343a2270617373223b733a36343a2238353333313633306663613262363763323334623662353765376166666339343033643632636631383639383963373136373539353665336363633261323064223b733a383a22757365726e616d65223b733a363a22626172626572223b733a363a2262616e6e6564223b733a313a2230223b733a31303a226c6173745f6c6f67696e223b733a31393a22323031362d31302d31312031373a32313a3335223b733a31333a226c6173745f6163746976697479223b733a31393a22323031362d31302d31312031373a32313a3335223b733a31323a22646174655f63726561746564223b733a31393a22323031362d30372d33302032333a33363a3333223b733a31303a22666f72676f745f657870223b4e3b733a31333a2272656d656d6265725f74696d65223b733a31393a22323031362d30382d30332030303a30303a3030223b733a31323a2272656d656d6265725f657870223b733a31363a2270715775686b7255737679397a4a4353223b733a31373a22766572696669636174696f6e5f636f6465223b4e3b733a31313a22746f74705f736563726574223b4e3b733a31303a2269705f61646472657373223b733a393a223132372e302e302e31223b7d),
('6a69624cb5d131d018565cba85342eab86d6dc1a', '127.0.0.1', 1476207118, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437363230373131383b69647c733a313a2232223b757365726e616d657c733a363a22626172626572223b656d61696c7c733a31383a226c696c6d6f70617440676d61696c2e636f6d223b6c6f67676564696e7c623a313b75736572696e666f7c4f3a383a22737464436c617373223a31343a7b733a323a226964223b733a313a2232223b733a353a22656d61696c223b733a31383a226c696c6d6f70617440676d61696c2e636f6d223b733a343a2270617373223b733a36343a2238353333313633306663613262363763323334623662353765376166666339343033643632636631383639383963373136373539353665336363633261323064223b733a383a22757365726e616d65223b733a363a22626172626572223b733a363a2262616e6e6564223b733a313a2230223b733a31303a226c6173745f6c6f67696e223b733a31393a22323031362d31302d31312031373a32313a3335223b733a31333a226c6173745f6163746976697479223b733a31393a22323031362d31302d31312031373a32313a3335223b733a31323a22646174655f63726561746564223b733a31393a22323031362d30372d33302032333a33363a3333223b733a31303a22666f72676f745f657870223b4e3b733a31333a2272656d656d6265725f74696d65223b733a31393a22323031362d30382d30332030303a30303a3030223b733a31323a2272656d656d6265725f657870223b733a31363a2270715775686b7255737679397a4a4353223b733a31373a22766572696669636174696f6e5f636f6465223b4e3b733a31313a22746f74705f736563726574223b4e3b733a31303a2269705f61646472657373223b733a393a223132372e302e302e31223b7d6d73677c733a3231373a223c64697620636c6173733d22616c65727420616c6572742d7375636365737320616c6572742d6469736d69737369626c65223e0a202020202020202020202020202020203c627574746f6e20747970653d22627574746f6e2220636c6173733d22636c6f73652220646174612d6469736d6973733d22616c6572742220617269612d68696464656e3d2274727565223ec3973c2f627574746f6e3e0a2020202020202020202020202020202053616c65206164646564207375636365737366756c6c79210a20202020202020202020202020203c2f6469763e223b5f5f63695f766172737c613a323a7b733a333a226d7367223b733a333a226f6c64223b733a31303a2273616c656974656d6964223b733a333a226f6c64223b7d73616c656974656d69647c733a323a223335223b),
('cbd50953e8da5d254a2e149cd11159a0e9fbdefc', '127.0.0.1', 1476212292, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437363231323239323b69647c733a313a2232223b757365726e616d657c733a363a22626172626572223b656d61696c7c733a31383a226c696c6d6f70617440676d61696c2e636f6d223b6c6f67676564696e7c623a313b75736572696e666f7c4f3a383a22737464436c617373223a31343a7b733a323a226964223b733a313a2232223b733a353a22656d61696c223b733a31383a226c696c6d6f70617440676d61696c2e636f6d223b733a343a2270617373223b733a36343a2238353333313633306663613262363763323334623662353765376166666339343033643632636631383639383963373136373539353665336363633261323064223b733a383a22757365726e616d65223b733a363a22626172626572223b733a363a2262616e6e6564223b733a313a2230223b733a31303a226c6173745f6c6f67696e223b733a31393a22323031362d31302d31312031373a32313a3335223b733a31333a226c6173745f6163746976697479223b733a31393a22323031362d31302d31312031373a32313a3335223b733a31323a22646174655f63726561746564223b733a31393a22323031362d30372d33302032333a33363a3333223b733a31303a22666f72676f745f657870223b4e3b733a31333a2272656d656d6265725f74696d65223b733a31393a22323031362d30382d30332030303a30303a3030223b733a31323a2272656d656d6265725f657870223b733a31363a2270715775686b7255737679397a4a4353223b733a31373a22766572696669636174696f6e5f636f6465223b4e3b733a31313a22746f74705f736563726574223b4e3b733a31303a2269705f61646472657373223b733a393a223132372e302e302e31223b7d6d73677c733a3231373a223c64697620636c6173733d22616c65727420616c6572742d7375636365737320616c6572742d6469736d69737369626c65223e0a202020202020202020202020202020203c627574746f6e20747970653d22627574746f6e2220636c6173733d22636c6f73652220646174612d6469736d6973733d22616c6572742220617269612d68696464656e3d2274727565223ec3973c2f627574746f6e3e0a2020202020202020202020202020202053616c65206164646564207375636365737366756c6c79210a20202020202020202020202020203c2f6469763e223b5f5f63695f766172737c613a323a7b733a333a226d7367223b733a333a226e6577223b733a31303a2273616c656974656d6964223b733a333a226e6577223b7d73616c656974656d69647c733a323a223430223b),
('5c1708ad895d8dd655a2752db838522bb77a3429', '127.0.0.1', 1476391198, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437363339313139383b69647c733a313a2232223b757365726e616d657c733a353a2261646d696e223b656d61696c7c733a31383a226c696c6d6f70617440676d61696c2e636f6d223b6c6f67676564696e7c623a313b75736572696e666f7c4f3a383a22737464436c617373223a31343a7b733a323a226964223b733a313a2232223b733a353a22656d61696c223b733a31383a226c696c6d6f70617440676d61696c2e636f6d223b733a343a2270617373223b733a36343a2238353333313633306663613262363763323334623662353765376166666339343033643632636631383639383963373136373539353665336363633261323064223b733a383a22757365726e616d65223b733a353a2261646d696e223b733a363a2262616e6e6564223b733a313a2230223b733a31303a226c6173745f6c6f67696e223b733a31393a22323031362d31302d31312031393a31323a3136223b733a31333a226c6173745f6163746976697479223b733a31393a22323031362d31302d31312031393a31323a3136223b733a31323a22646174655f63726561746564223b733a31393a22323031362d30372d33302032333a33363a3333223b733a31303a22666f72676f745f657870223b4e3b733a31333a2272656d656d6265725f74696d65223b733a31393a22323031362d30382d30332030303a30303a3030223b733a31323a2272656d656d6265725f657870223b733a31363a2270715775686b7255737679397a4a4353223b733a31373a22766572696669636174696f6e5f636f6465223b4e3b733a31313a22746f74705f736563726574223b4e3b733a31303a2269705f61646472657373223b733a393a223132372e302e302e31223b7d),
('73ca6f78c61f990f3a12dd76d3f49ae2f6dd0c61', '127.0.0.1', 1476213136, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437363231333133353b69647c733a313a2232223b757365726e616d657c733a353a2261646d696e223b656d61696c7c733a31383a226c696c6d6f70617440676d61696c2e636f6d223b6c6f67676564696e7c623a313b75736572696e666f7c4f3a383a22737464436c617373223a31343a7b733a323a226964223b733a313a2232223b733a353a22656d61696c223b733a31383a226c696c6d6f70617440676d61696c2e636f6d223b733a343a2270617373223b733a36343a2238353333313633306663613262363763323334623662353765376166666339343033643632636631383639383963373136373539353665336363633261323064223b733a383a22757365726e616d65223b733a353a2261646d696e223b733a363a2262616e6e6564223b733a313a2230223b733a31303a226c6173745f6c6f67696e223b733a31393a22323031362d31302d31312031393a31323a3136223b733a31333a226c6173745f6163746976697479223b733a31393a22323031362d31302d31312031393a31323a3136223b733a31323a22646174655f63726561746564223b733a31393a22323031362d30372d33302032333a33363a3333223b733a31303a22666f72676f745f657870223b4e3b733a31333a2272656d656d6265725f74696d65223b733a31393a22323031362d30382d30332030303a30303a3030223b733a31323a2272656d656d6265725f657870223b733a31363a2270715775686b7255737679397a4a4353223b733a31373a22766572696669636174696f6e5f636f6465223b4e3b733a31313a22746f74705f736563726574223b4e3b733a31303a2269705f61646472657373223b733a393a223132372e302e302e31223b7d),
('983ea69b1b0b5776e931f0838b1546c0d0de0b50', '127.0.0.1', 1476391500, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437363339313230373b),
('1c00e15a4e8b2ec98d376847191f841b8d8795b0', '127.0.0.1', 1477065343, 0x5f5f63695f6c6173745f726567656e65726174657c693a313437373036353334333b);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(300) NOT NULL,
  `phone` varchar(12) NOT NULL,
  `gender` enum('male','female') NOT NULL,
  `address` varchar(500) NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `status` enum('active','deleted') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `first_name`, `last_name`, `email`, `phone`, `gender`, `address`, `created_by`, `created_at`, `status`) VALUES
(2, 'Samuel', 'Armah', 'lilmopat@gmail.com', '+23324829182', 'male', 'accra\r\n2332', '2', '2016-08-12 05:00:00', 'active'),
(3, 'Samuel', 'Armah', 'lilmopat@gmail.com', '+23324829182', 'male', 'accra\r\n2332', '2', '2016-08-11 05:15:13', 'active'),
(4, 'Caroline', 'Ramey', 'iteearmah@gmail.com', '15218612944', 'female', '15155 OCEANVIEW DR', '2', '2016-08-13 06:10:24', 'active');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` int(11) NOT NULL,
  `tender` double NOT NULL,
  `change_amount` double NOT NULL,
  `service_person` int(11) NOT NULL,
  `sales_by` int(11) NOT NULL,
  `sales_at` datetime NOT NULL,
  `sales_to` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `tender`, `change_amount`, `service_person`, `sales_by`, `sales_at`, `sales_to`) VALUES
(4, 10, 5, 1, 2, '2016-08-12 06:20:58', 0),
(5, 10, 5, 1, 2, '2016-08-12 06:31:10', 0),
(7, 13, 8, 1, 2, '2016-08-12 06:54:21', 0),
(8, 20, 10, 1, 2, '2016-08-12 06:54:50', 2),
(9, 20, 10, 1, 2, '2016-08-12 07:09:16', 3),
(10, 10, 5, 1, 2, '2016-08-14 15:53:38', 4),
(11, 10, 5, 2, 2, '2016-08-14 15:55:46', 0),
(12, 10, 5, 1, 2, '2016-08-14 15:57:16', 2),
(13, 10, 5, 1, 2, '2016-08-14 15:59:35', 3),
(14, 20, 15, 2, 2, '2016-08-14 16:29:07', 0),
(15, 20, 15, 1, 2, '2016-08-14 16:31:17', 0),
(16, 20, 15, 1, 2, '2016-08-14 16:37:27', 0),
(17, 50, 40, 4, 2, '2016-08-14 18:01:20', 0),
(18, 70, 60, 4, 2, '2016-08-14 18:01:55', 0),
(20, 20, 15, 1, 2, '2016-08-21 07:39:32', 0),
(21, 20, 15, 1, 7, '2016-08-23 10:59:13', 0),
(22, 20, 15, 1, 2, '2016-08-25 13:46:50', 0),
(23, 10, 5, 1, 2, '2016-08-25 13:49:38', 0),
(24, 20, 15, 1, 2, '2016-08-25 16:17:35', 0),
(25, 22, 17, 1, 2, '2016-08-25 17:24:32', 0),
(26, 10, 5, 1, 7, '2016-10-11 17:21:19', 0),
(27, 10, 5, 1, 2, '2016-10-11 17:25:34', 0),
(28, 10, 5, 1, 2, '2016-10-11 17:25:45', 0),
(29, 100, 95, 1, 2, '2016-10-11 17:26:52', 0),
(30, 10, 5, 2, 2, '2016-10-11 17:27:33', 0),
(31, 10, 5, 1, 2, '2016-10-11 17:29:11', 0),
(32, 10, 5, 1, 2, '2016-10-11 17:30:43', 0),
(33, 10, 5, 1, 2, '2016-10-11 17:30:59', 0),
(34, 100, 95, 1, 2, '2016-10-11 17:31:09', 0),
(35, 10, 5, 1, 2, '2016-10-11 17:31:28', 0),
(36, 10, 5, 1, 2, '2016-10-11 17:31:58', 0),
(37, 10, 5, 1, 2, '2016-10-11 17:32:53', 0),
(38, 10, 5, 1, 2, '2016-10-11 17:34:19', 0),
(39, 12, 7, 1, 2, '2016-10-11 17:35:13', 2),
(40, 20, 10, 1, 2, '2016-10-11 17:35:49', 4);

-- --------------------------------------------------------

--
-- Table structure for table `sale_items`
--

CREATE TABLE `sale_items` (
  `id` int(11) NOT NULL,
  `sales_id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `sale_type` enum('cash','credit','administrative','mobilemoney') NOT NULL DEFAULT 'cash',
  `amount` double NOT NULL,
  `service_id` int(11) NOT NULL,
  `reference` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sale_items`
--

INSERT INTO `sale_items` (`id`, `sales_id`, `customer_id`, `sale_type`, `amount`, `service_id`, `reference`) VALUES
(2, 4, 0, 'cash', 5, 1, ''),
(3, 5, 0, 'cash', 5, 1, ''),
(5, 7, 0, 'cash', 5, 1, ''),
(6, 8, 2, 'cash', 10, 5, ''),
(7, 9, 3, 'cash', 10, 5, ''),
(8, 10, 4, 'cash', 5, 1, ''),
(9, 11, 0, 'cash', 5, 1, ''),
(10, 12, 2, 'cash', 5, 1, ''),
(11, 13, 3, 'cash', 5, 1, ''),
(12, 14, 0, 'cash', 5, 1, ''),
(13, 14, 0, 'cash', 5, 1, ''),
(14, 15, 0, 'cash', 5, 1, ''),
(15, 15, 0, 'cash', 5, 1, ''),
(16, 16, 0, 'cash', 5, 1, ''),
(17, 17, 0, 'cash', 10, 5, ''),
(18, 18, 0, 'cash', 10, 5, ''),
(20, 20, 0, 'mobilemoney', 5, 1, ''),
(21, 21, 0, 'cash', 5, 1, ''),
(22, 22, 0, 'mobilemoney', 5, 1, ''),
(23, 23, 0, 'mobilemoney', 5, 1, 'lindapayment'),
(24, 24, 0, 'mobilemoney', 5, 1, 'lindapayment'),
(25, 25, 0, 'cash', 5, 1, ''),
(26, 26, 0, 'cash', 5, 1, ''),
(27, 27, 0, 'cash', 5, 1, ''),
(28, 28, 0, 'cash', 5, 1, ''),
(29, 29, 0, 'cash', 5, 1, ''),
(30, 30, 0, 'cash', 5, 1, ''),
(31, 31, 0, 'cash', 5, 1, ''),
(32, 32, 0, 'cash', 5, 1, ''),
(33, 33, 0, 'cash', 5, 1, ''),
(34, 34, 0, 'cash', 5, 1, ''),
(35, 35, 0, 'cash', 5, 1, ''),
(36, 36, 0, 'cash', 5, 1, ''),
(37, 37, 0, 'cash', 5, 1, ''),
(38, 38, 0, 'cash', 5, 1, ''),
(39, 39, 2, 'cash', 5, 1, ''),
(40, 40, 4, 'cash', 10, 5, '');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(11) NOT NULL,
  `service_name` varchar(255) NOT NULL,
  `service_price` decimal(10,2) NOT NULL,
  `service_type` enum('adults','children','all') NOT NULL,
  `created_at` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `service_name`, `service_price`, `service_type`, `created_at`, `created_by`, `status`) VALUES
(1, 'Adult Cut', '5.00', 'children', '2016-07-31 17:57:53', 2, 'active'),
(5, 'Shaving', '10.00', 'adults', '2016-08-02 20:35:14', 2, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `service_person`
--

CREATE TABLE `service_person` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `address` varchar(255) NOT NULL,
  `started_work` date NOT NULL,
  `terminated_work` date NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_person`
--

INSERT INTO `service_person` (`id`, `name`, `phone`, `address`, `started_work`, `terminated_work`, `status`, `created_by`, `created_at`) VALUES
(1, 'Barber One', '248291829', 'c/o Christiana Armah Church of Pentecost', '2016-08-07', '2016-08-08', 'active', 2, '2016-08-07 10:31:20'),
(2, 'Barber Two', '0244282839', 'Accra', '2016-08-06', '2016-08-03', 'active', 2, '2016-08-07 10:56:24'),
(3, 'Barber Three', '', '', '0000-00-00', '0000-00-00', 'active', 2, '2016-08-13 08:33:39'),
(4, 'Gloria', '024edgdhyhdhjdh', 'fdd', '2016-08-15', '2016-08-17', 'active', 2, '2016-08-14 17:58:41');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `company_name` varchar(255) DEFAULT NULL,
  `slogan` varchar(255) DEFAULT NULL,
  `address` text,
  `telephone` text,
  `location` varchar(255) DEFAULT NULL,
  `email` text,
  `currency` varchar(10) DEFAULT NULL,
  `VAT` decimal(10,1) DEFAULT NULL,
  `NHIL` decimal(10,1) DEFAULT NULL,
  `receipt_footer` text,
  `logo_filename` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `company_name`, `slogan`, `address`, `telephone`, `location`, `email`, `currency`, `VAT`, `NHIL`, `receipt_footer`, `logo_filename`) VALUES
(1, 'Barbar Shop', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aauth_groups`
--
ALTER TABLE `aauth_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aauth_group_to_group`
--
ALTER TABLE `aauth_group_to_group`
  ADD PRIMARY KEY (`group_id`,`subgroup_id`);

--
-- Indexes for table `aauth_login_attempts`
--
ALTER TABLE `aauth_login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aauth_perms`
--
ALTER TABLE `aauth_perms`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aauth_perm_to_group`
--
ALTER TABLE `aauth_perm_to_group`
  ADD PRIMARY KEY (`perm_id`,`group_id`);

--
-- Indexes for table `aauth_perm_to_user`
--
ALTER TABLE `aauth_perm_to_user`
  ADD PRIMARY KEY (`perm_id`,`user_id`);

--
-- Indexes for table `aauth_pms`
--
ALTER TABLE `aauth_pms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `full_index` (`id`,`sender_id`,`receiver_id`,`date_read`);

--
-- Indexes for table `aauth_users`
--
ALTER TABLE `aauth_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aauth_user_to_group`
--
ALTER TABLE `aauth_user_to_group`
  ADD PRIMARY KEY (`user_id`,`group_id`);

--
-- Indexes for table `aauth_user_variables`
--
ALTER TABLE `aauth_user_variables`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id_index` (`user_id`);

--
-- Indexes for table `audit_trail`
--
ALTER TABLE `audit_trail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sale_items`
--
ALTER TABLE `sale_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_person`
--
ALTER TABLE `service_person`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aauth_groups`
--
ALTER TABLE `aauth_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `aauth_login_attempts`
--
ALTER TABLE `aauth_login_attempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `aauth_perms`
--
ALTER TABLE `aauth_perms`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `aauth_pms`
--
ALTER TABLE `aauth_pms`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `aauth_users`
--
ALTER TABLE `aauth_users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `aauth_user_variables`
--
ALTER TABLE `aauth_user_variables`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `audit_trail`
--
ALTER TABLE `audit_trail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `sale_items`
--
ALTER TABLE `sale_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `service_person`
--
ALTER TABLE `service_person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
