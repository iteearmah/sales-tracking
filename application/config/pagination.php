<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$config['full_tag_open'] = '<ul class="pagination">';
$config['full_tag_close'] = '</ul>';
$config['first_link'] = FALSE;
$config['first_tag_open'] = '<li>';
$config['first_tag_close'] = '<li>';

$config['last_link'] = FALSE;
$config['last_tag_open'] = '<li>';
$config['last_tag_close'] = '</li>';

$config['next_link'] = '&rarr;';
$config['next_tag_open'] = '<li class="next">';
$config['next_tag_close'] = '</li>';

$config['prev_link'] = '&larr;';
$config['prev_tag_open'] = '<li class="prev">';
$config['prev_tag_close'] = '</li>';

$config['cur_tag_open'] = '<li><span>';
$config['cur_tag_close'] = '</span></li>';

$config['num_tag_open'] = '<li>';
$config['num_tag_close'] = '</li>';
$config['attributes'] = array('class' => 'pagination');
$config['use_page_numbers'] = true;
//$config['uri_segment'] = 2;
$config['enable_query_strings'] = true;
//$config['Preffix'] = '?taxonomy=category';
$config['page_query_string'] = true;
$config['query_string_segment'] = 'page';