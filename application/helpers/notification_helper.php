<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
if(!function_exists('notification')){
	function notification($msg='',$type='warning',$title='Alert!')
	{
		return '<div class="alert alert-'.$type.' alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                '.$msg.'
              </div>';
	}
}