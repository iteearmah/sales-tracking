<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('listTable'))
{
	function listTable($items)
	{
		$output='';
		$output.='<table class="table table-striped">'."\n";
		$output.='<tbody>'."\n";
		$output.='<tr><th>i/o</th><th>Username</th><th>Email</th><th>Last Activity</th><th></th></tr>'."\n";
		foreach ($items as $item)
		{
			$last_activity=($item['last_activity']!='0000-00-00' && $item['last_activity']!=NULL)? gmdate('F j, Y g:i a',strtotime($item['last_activity'])):'';
			$output.='<tr>
			<td>'.$item['id'].'</td>
			<td>'.$item['username'].'</td>
			<td><a href="mailto:'.$item['username'].'">'.$item['email'].'</a></td>
			<td>'.$last_activity.'</td>
			<td><a href="'.base_url('users/edit/'.$item['id']).'" class="btn btn-default btn-flat" title="Edit"><i class="fa fa-edit"></i></a>  <a href="'.base_url('users/delete/'.$item['id']).'" class="btn  btn-danger btn-flat" title="Delete"><i class="fa fa-trash"></i></a></td></tr>'."\n";
		}
		$output.='</tbody>'."\n";
		$output.='</table>'."\n";
		return $output;
	}
}
