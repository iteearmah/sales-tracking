<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('listTable'))
{
	function listTable($items)
	{
		$output='';
		$output.='<table class="table table-striped">'."\n";
		$output.='<tbody>'."\n";
		$output.='<tr><th>First Name</th><th>Last Name</th><th>Phone</th><th>Email</th><th>Gender</th><th></th></tr>'."\n";
		foreach ($items as $item)
		{
			$output.='<tr>
					<td>'.$item['first_name'].'</td>
					<td>'.$item['last_name'].'</td>
					<td> <a href="tel:'.$item['phone'].'" title="click to call">'.$item['phone'].'</a></td>
					<td> <a href="mailto:'.$item['email'].'" title="click to send email">'.$item['email'].'</a></td>
					<td>'.ucwords($item['gender']).'</td>
					<td><a href="'.base_url('customers/edit/'.$item['id']).'" class="btn btn-default btn-flat" title="Edit"><i class="fa fa-edit"></i></a>  <a href="'.base_url('customers/delete/'.$item['id']).'" class="btn  btn-danger btn-flat" title="Delete"><i class="fa fa-trash"></i></a></td></tr>'."\n";
		}
		$output.='</tbody>'."\n";
		$output.='</table>'."\n";
		return $output;
	}
}
