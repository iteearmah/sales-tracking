<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property CI_DB $db 
 */
class Categories_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Get all categories
     * 
     * @return array
     */
    public function getAll() {
        $this->db->select('id,name');
        $this->db->from('categories');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function list_categories($limit, $offset, $search_term = '') {
        $query = $this->db->select('SQL_CACHE *', FALSE)
                ->from('categories')
                ->limit($limit, $offset)
                ->order_by('name', 'ASC');
        if ($search_term != '') {
            $this->db->where("(`name` LIKE '%$search_term%')");
        }
        $rows['rows'] = $query->get()->result_array();

        $query = $this->db->select('COUNT(*)  as count', FALSE)
                ->from('categories');
        if ($search_term != '') {
            $this->db->where("(`name` LIKE '%$search_term%')");
        }

        $tmp = $query->get()->result();

        $rows['num_rows'] = $tmp[0]->count;

        return $rows;
    }

    public function save($data) {
        if ($data) {
            if ($this->db->insert('categories', $data)) {
                return $this->db->insert_id();
            }
            return true;
        }
        return false;
    }

    public function getCategory($id) {
        if ($id) {
            $this->db->from('categories');
            $this->db->where('id', $id);
            $query = $this->db->get();
            $row = $query->row_array();
            return $row;
        } else {
            return FALSE;
        }
    }

    public function updateCategory($id, $data) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->update('categories', $data);
        } else {
            return FALSE;
        }
    }

}
