<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_model extends CI_Model {

    public $variable;

    public function __construct() {
        parent::__construct();
    }

    public function saveSales($data) {
        if ($data) {
            if ($this->db->insert('sales', $data)) {
                return $this->db->insert_id();
            }
            return true;
        }
        return false;
    }

    public function getSalesInfo($salesid) {
        if ($salesid) {
            $this->db->from('sales');
            $this->db->where('id', $salesid);
            $query = $this->db->get();
            $row = $query->row_array();
            return $row;
        } else {
            return FALSE;
        }
    }

    public function saveSaleItem($data) {
        if ($data) {
            if ($this->db->insert('sale_items', $data)) {
                return $this->db->insert_id();
            }
            return true;
        }
        return false;
    }

    public function getLatestSales($limit = 5, $userId = NULL) {
        $this->db->from('sales');
        $this->db->where('sales_at >=', gmdate('Y-m-d') . ' 00:00:00');
        
        if($userId){
            $this->db->where('sales_by', $userId);
        }
        
        $this->db->order_by('sales.id', 'DESC');
        $this->db->limit($limit);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function totalSales() {
        $this->db->select('SUM(sale_items.amount) AS total');
        $this->db->from('sales');
        $this->db->join('sale_items', 'sale_items.sales_id = sales.id', 'left');
        $this->db->join('services', 'services.id = sale_items.service_id', 'left');
        $query = $this->db->get();
        $row = $query->row_array();
        return $row['total'];
    }

    public function Sales_report($start_date = '', $end_date = '', $category_id = '', $service = '', $customer = '', $serviceperson = '') {
        $this->db->select('sales.id AS ID,sales.service_person,services.service_name,categories.name as category_name, sales.sales_at,sale_items.sale_type as payment_type,sale_items.amount,sales.tender,customers.id AS customerid,customers.first_name,customers.last_name,customers.phone');
        $this->db->from('sales');
        $this->db->join('sale_items', 'sale_items.sales_id = sales.id', 'left');
        $this->db->join('services', 'services.id = sale_items.service_id', 'left');
        $this->db->join('customers', 'customers.id = sale_items.customer_id', 'left');
        $this->db->join('categories', 'services.category_id = categories.id', 'left');
        $this->db->where("DATE(sales.sales_at) >='$start_date' AND DATE(sales.sales_at) <= '$end_date'");
        if ($category_id != '') {
            $this->db->where('services.category_id', $category_id);
        }
        if ($service != '') {
            $this->db->where('services.id', $service);
        }
        if ($customer != '') {
            $this->db->where('customers.id', $customer);
        }
        if ($serviceperson != '') {
            $this->db->where('sales.service_person', $serviceperson);
        }
        //$this->db->where("sales.sales_at BETWEEN '$start_date' AND '$end_date'");
        $this->db->order_by('sales.sales_at', 'DESC');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function Sales_receipt($salesid) {
        $this->db->select('sales.id AS ID,services.service_name,sales.sales_at,sale_items.amount,customers.first_name,customers.last_name,customers.phone');
        $this->db->from('sales');
        $this->db->join('sale_items', 'sale_items.sales_id = sales.id', 'left');
        $this->db->join('services', 'services.id = sale_items.service_id', 'left');
        $this->db->join('customers', 'customers.id = sale_items.customer_id', 'left');
        $this->db->where('sales.id', $salesid);
        $query = $this->db->get();
        return $query->result_array();
    }

    public function Sales_Customer($salesid) {
        $this->db->select('sales.id AS ID,sales.sales_at,customers.id AS customerid,customers.first_name,customers.last_name,customers.phone');
        $this->db->from('sales');
        $this->db->join('customers', 'customers.id = sales.sales_to', 'left');
        $this->db->where('sales.id', $salesid);
        $query = $this->db->get();
        return $query->row_array();
    }

    public function Sales_by($salesid) {
        $this->db->select('sales.id AS ID,sales.sales_at,aauth_users.username');
        $this->db->from('sales');
        $this->db->join('aauth_users', 'aauth_users.id = sales.sales_by', 'left');
        $this->db->where('sales.id', $salesid);
        $query = $this->db->get();
        return $query->row_array();
    }

}

/* End of file Sales_model.php */
/* Location: ./application/models/Sales_model.php */