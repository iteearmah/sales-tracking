<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Serviceperson_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function saveServicePerson($data)
	{
		if($data)
		{
			if($this->db->insert('service_person', $data))
			{
				return $this->db->insert_id();
			}
			return true;
		}
		return false;
	}

	public function getServicePerson($id)
	{
		if($id)
  	{
  		$this->db->from('service_person');
			$this->db->where('id',$id);
	    $query = $this->db->get();
	    $row = $query->row_array();
	    return $row;
  	}
  	else
  	{
  		return FALSE;
  	}
	}

	public function updateServicePerson($id,$data)
	{
		if($id)
  	{
  		$this->db->where('id', $id);
			return $this->db->update('service_person', $data);
  	}
  	else
  	{
  		return FALSE;
  	}
	}

	public function list_persons($limit, $offset,$search_term='')
	{
	
		$query = $this->db->select('SQL_CACHE *', FALSE)
			->from('service_person')
			->limit($limit, $offset)
			->order_by('name', 'ASC')
			->where_in('status', array('active','inactive'));
       if($search_term!='')
			{
				$this->db->where("(`name` LIKE '%$search_term%')");
			}
		$rows['rows'] = $query->get()->result_array();
		
		$query = $this->db->select('COUNT(*)  as count', FALSE)

			->from('service_person')
			->where_in('status', array('active','inactive'));
			if($search_term!='')
			{
				$this->db->where("(`name` LIKE '%$search_term%')");
			}
			
		$tmp = $query->get()->result();
		
		$rows['num_rows'] = $tmp[0]->count;
		
		return $rows;
	}

	public function getServicePersons()
	{
		$this->db->from('service_person');
		$this->db->where('status', 'active');
		$query = $this->db->get();
	  $rows = $query->result_array();
	  return $rows;
	}

	public function totalNumServicePersons()
	{
		$query = $this->db->select("COUNT(*) AS total", FALSE);
		$this->db->from('service_person');
		$query = $this->db->get();
	  $row = $query->row_array();
	  return $row['total'];
	}

}

/* End of file Serviceperson_model.php */
/* Location: ./application/models/Serviceperson_model.php */