<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Template {
    protected $data;
    protected $template;
	public function __construct()
    {
        global $platform;
        $this->_CI =& get_instance();
        $this->_CI->load->library("Aauth");
        $this->template = $this->_CI->config->item('default_template');
        $this->mobile_template = $this->_CI->config->item('mobile_template');
        
    }

    public function default_tmpl($view,$data=array())
    {
        $this->desktop_template($view,$data);
    }

    public function desktop_template($view,$data=array())
    {
        $this->data=$data;
        $this->data['is_admin']=$this->_CI->aauth->is_admin();
        $this->_CI->load->view($this->template.'/header',$this->data);
        $this->_CI->load->view($this->template.'/left-sidebar',$this->data);
        $this->_CI->load->view($this->template.'/'.$view,$this->data);
        $this->_CI->load->view($this->template.'/footer',$this->data);
    }

    public function default_plain($view,$data=array())
    {
        $this->data=$data;
        $this->_CI->load->view($this->template.'/'.$view,$this->data);
    }
    
} 