<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library("Aauth");
		$this->load->library('form_validation');
		$this->load->helper('form');
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$data['msg']='';
	}

	public function index()
	{
		$data=array();
		$data['msg']='';
		$rememberme=FALSE;
		if($this->form_validation->run())
		{
		 	$username=$this->input->post('username');
		 	$password=$this->input->post('password');
		 	 if($this->aauth->login($username, $password))
		 	 {

		 	 	$this->session->set_userdata('userinfo',$this->aauth->get_user());
		 	 		if($this->aauth->is_admin())
		 	 		{
		 	 			redirect('/dashboard/','location');
		 	 		}
		 	 		else
		 	 		{
		 	 			redirect('/sales','location');
		 	 		}
		 	 }
		 	 else
		 	 {
		 	 	$err=$this->aauth->get_errors_array();
	 			$data['msg']=notification($err[0],'error');
	 			$this->template->default_plain('login',$data);
		 	 }

		}
		else
		{
			$this->template->default_plain('login',$data);
		}
		
	}

	 public function logout() 
	 {
    $this->aauth->logout();
    redirect('login','location');
   }

}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */