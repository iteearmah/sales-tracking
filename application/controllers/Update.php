<?php

class Update extends CI_Controller {

    public function index() {
        // Run git pull
        shell_exec("git pull origin master");

        // Update database
        $this->load->library('migration');

        if ($this->migration->current() === FALSE) {
            show_error($this->migration->error_string());
        }

        $this->session->set_flashdata('msg', notification("Software has been updated!", 'success'));
        redirect('dashboard');
    }

}
