<?php

/**
 * @property Stock_model $Stock_model
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends CI_Controller {

    private $data = array();
    private $pagelist;
    private $limit = 10;
    private $offset = 0;

    public function __construct() {
        parent::__construct();
        //$this->output->enable_profiler(TRUE);
        $this->data['msg'] = '';
        $this->load->library("Aauth");
        $this->load->library('form_validation');
        $this->load->model('Stock_model');
        $this->load->model('Services_model');
        $this->load->helper('form');
        $this->load->helper('inventory');
//        $this->load->helper('services');
        $this->data['serviceInfo'] = array('price' => '', 'type' => '', 'name' => '');
        $this->form_validation->set_error_delimiters('<div class="alert alert-error alert-dismissible">', '</div>');
        if ($this->aauth->is_loggedin() === false) {
            redirect('login', 'location');
        }

        if (!$this->aauth->is_admin()) {
            redirect('sales', 'location');
        }

        $this->data['userinfo'] = $this->session->userdata('userinfo');
        $this->data['serviceSelected'] = '';
        $this->data['quantity'] = '';
        $this->data['pagelist'] = '';
        $this->data['search_term'] = '';
        $this->data['servicedata'] = array();
        $this->form_validation->set_rules('service', 'Item', 'required');
        $this->form_validation->set_rules('quantity', 'Quantity', 'required');
        $this->data['readonly'] = false;
    }

    public function index() {
        if ($this->input->get('page')) {
            $this->page = $this->input->get('page');
            $this->offset = ($this->page == 1) ? 0 : ($this->page * $this->limit) - $this->limit;
        }
        $results = $this->Stock_model->listItems($this->limit, $this->offset, $this->data['search_term']);
        $this->pagelist = $results['rows'];
        $this->data['pagelist'] = listTable($this->pagelist);
        $this->data['num_results'] = $results['num_rows'];
        $this->load->library('pagination');
        //$this->config->load('pagination');
        $this->pagnconfig['base_url'] = base_url('services');
        $this->pagnconfig['total_rows'] = $this->data['num_results'];
        $this->pagnconfig['per_page'] = $this->limit;
        $choice = $this->pagnconfig["total_rows"] / $this->pagnconfig["per_page"];
        $this->pagination->initialize($this->pagnconfig);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->template->default_tmpl('inventory', $this->data);
    }

    public function add() {
        $userinfo = $this->data['userinfo'];
        $serviceDataItems = array();
        $serviceData = $this->Services_model->getInventoryItems();
        $serviceDataItems[''] = 'Select';
        foreach ($serviceData as $item) {
            $serviceDataItems[$item['id']] = $item['service_name'];
        }
        $this->data['servicedata'] = $serviceDataItems;
        if ($this->form_validation->run()) {
            $service = $this->input->post('service');
            $quantity = $this->input->post('quantity');
            $this->data['serviceSelected'] = $service;
            $inputdata = array(
                'item_id' => $service,
                'quantity' => $quantity,
                'created_at' => gmdate('Y-m-d H:i:s')
            );
            $insert = $this->Stock_model->saveOrUpdate($service, $inputdata);
            if ($insert) {
                $this->session->set_flashdata('msg', notification("Item has been stocked successfully!", 'success'));
                redirect('inventory/edit/' . $insert, 'location');
            }
        }
        $this->template->default_tmpl('inventory-entry', $this->data);
    }
    
    public function addUsed() {
        $userId = $this->data['userinfo']->id;
        $serviceDataItems = array();
        $serviceData = $this->Services_model->getInventoryItems();
        $serviceDataItems[''] = 'Select';
        foreach ($serviceData as $item) {
            $serviceDataItems[$item['id']] = $item['service_name'];
        }
        $this->data['servicedata'] = $serviceDataItems;
        if ($this->form_validation->run()) {
            $service = $this->input->post('service');
            $quantity = $this->input->post('quantity');
            $this->data['serviceSelected'] = $service;
            $inputdata = array(
                'item_id' => $service,
                'quantity' => $quantity,
                'created_at' => gmdate('Y-m-d H:i:s'),
                'user_id' => $userId
            );
            $insert = $this->Stock_model->saveOrUpdateUsed($service, $inputdata);
            if ($insert) {
                $this->session->set_flashdata('msg', notification("Item has been stocked successfully!", 'success'));
                redirect('inventory/editUsed/' . $insert, 'location');
            }
        }
        $this->template->default_tmpl('inventory-entry', $this->data);
    }

    public function edit($id = 0) {
        $this->data['readonly'] = true;
        $serviceDataItems = array();
        $serviceData = $this->Services_model->getInventoryItems();
        $serviceDataItems[''] = 'Select';
        foreach ($serviceData as $item) {
            $serviceDataItems[$item['id']] = $item['service_name'];
        }
        $this->data['servicedata'] = $serviceDataItems;
        if ($this->form_validation->run()) {
            $service = $this->input->post('service');
            $quantity = $this->input->post('quantity');
            $inputdata = array(
                'item_id' => $service,
                'quantity' => $quantity
            );
            if ($this->Stock_model->update($id, $inputdata)) {
                $this->session->set_flashdata('msg', notification("Item  updated successfully!", 'success'));
                redirect('inventory/edit/' . $id, 'location');
            }
        }
        $stockInfo = $this->Stock_model->get($id);
        if ($stockInfo == false) {
            $this->session->set_flashdata('msg', notification("Item not fetched", 'danger'));
        } else {
            $serviceInfo = $this->Services_model->getService($stockInfo['item_id']);
            $this->data['serviceSelected'] = $stockInfo['item_id'];
            $this->data['quantity'] = $stockInfo['quantity'];
        }

        $this->template->default_tmpl('inventory-entry', $this->data);
    }
    
    public function editUsed($id = 0) {
        $this->data['readonly'] = true;
        $serviceDataItems = array();
        $serviceData = $this->Services_model->getInventoryItems();
        $serviceDataItems[''] = 'Select';
        foreach ($serviceData as $item) {
            $serviceDataItems[$item['id']] = $item['service_name'];
        }
        $this->data['servicedata'] = $serviceDataItems;
        if ($this->form_validation->run()) {
            $service = $this->input->post('service');
            $quantity = $this->input->post('quantity');
            $inputdata = array(
                'item_id' => $service,
                'quantity' => $quantity
            );
            if ($this->Stock_model->updateUsed($id, $inputdata)) {
                $this->session->set_flashdata('msg', notification("Item  updated successfully!", 'success'));
                redirect('inventory/editUsed/' . $id, 'location');
            }
        }
        $stockInfo = $this->Stock_model->getUsed($id);
        if ($stockInfo == false) {
            $this->session->set_flashdata('msg', notification("Item not fetched", 'danger'));
        } else {
            $serviceInfo = $this->Services_model->getService($stockInfo['item_id']);
            $this->data['serviceSelected'] = $stockInfo['item_id'];
            $this->data['quantity'] = $stockInfo['quantity'];
        }

        $this->template->default_tmpl('inventory-entry', $this->data);
    }

//    public function delete($id = 0) {
//        $this->Services_model->updateService($id, array('status' => 'deleted'));
//        $this->session->set_flashdata('msg', notification("Service deleted successfully!", 'success'));
//        redirect('services', 'location');
//    }

}

/* End of file Services.php */
/* Location: ./application/controllers/Services.php */