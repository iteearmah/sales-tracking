<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends CI_Controller {
	private $data=array();
	private $limit=10;
	private $offset=0;
	public function __construct()
	{
		parent::__construct();
		$this->load->library("Aauth");
		$this->load->library('form_validation');
		$this->load->helper('customers');
		if($this->aauth->is_loggedin()===false)
		{
			redirect('login','location');
		}
		$this->load->model('Customer_model');
		$this->form_validation->set_error_delimiters('<div class="alert alert-error alert-dismissible">', '</div>');
	
		$this->data['userinfo']=$this->session->userdata('userinfo');
		$this->data['customerdata']=array();
		$this->data['msg']='';
		$this->data['first_name']='';
		$this->data['last_name']='';
		$this->data['email']='';
		$this->data['gender']='';
		$this->data['phone']='';
		$this->data['address']='';
		$this->data['pagination'] =$this->data['search_term']='';
	}

	public function index()
	{
		if($this->input->get('page'))
		{
			$this->page=$this->input->get('page');
			$this->offset = ($this->page  == 1) ? 0 : ($this->page * $this->limit) - $this->limit;
		}
		if($this->input->get('search'))
		{
			$this->data['search_term']=trim($this->input->get('search'));
		}
		$results = $this->Customer_model->list_customers($this->limit, $this->offset,$this->data['search_term']);
		$this->pagelist= $results['rows'];

		$this->data['pagelist'] = listTable($this->pagelist);
		$this->data['num_results'] = $results['num_rows'];
		$this->load->library('pagination');
		$this->pagnconfig=array();
		$this->pagnconfig['base_url'] = base_url('customers');
		$this->pagnconfig['total_rows'] = $this->data['num_results'];
		$this->pagnconfig['per_page'] = $this->limit;
		$this->pagnconfig['get']=(isset($_GET['search']))?'?search='.$_GET['search']:'';
		$this->pagnconfig['page_query_string']=TRUE;

		$choice = $this->pagnconfig["total_rows"] / $this->pagnconfig["per_page"];
		$this->pagination->initialize($this->pagnconfig);
		$this->data['pagination'] = $this->pagination->create_links();

		$this->template->default_tmpl('customers',$this->data);
		$this->config->load('pagination');
	}

	public function add()
	{
		$this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');
		if($this->form_validation->run())
		{
			$first_name=$this->input->post('first_name');
			$last_name=$this->input->post('last_name');
			$email=$this->input->post('email');
			$gender=$this->input->post('gender');
			$phone=$this->input->post('phone');
			$address=$this->input->post('address');
			$created_by=$this->data['userinfo']->id;
			$created_at=gmdate('Y-m-d H:i:s');
			$customerDBData=array('first_name'=>$first_name,'last_name'=>$last_name,'email'=>$email,'phone'=>$phone,'gender'=>$gender,'address'=>$address,'created_by'=>$created_by,'created_at'=>$created_at);
			$customerid=$this->Customer_model->saveCustomer($customerDBData);
			if($customerid)
			{
				$this->session->set_flashdata('msg', notification("Customer added successfully!",'success'));
				redirect('customers/edit/'.$customerid,'location');
			}
		}
		$this->template->default_tmpl('customers-entry',$this->data);
	}

	public function edit($id=0)
	{
		$this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');
		if($this->form_validation->run())
		{
			$first_name=$this->input->post('first_name');
			$last_name=$this->input->post('last_name');
			$email=$this->input->post('email');
			$gender=$this->input->post('gender');
			$phone=$this->input->post('phone');
			$address=$this->input->post('address');
			$customerDBData=array('first_name'=>$first_name,'last_name'=>$last_name,'email'=>$email,'phone'=>$phone,'gender'=>$gender,'address'=>$address);
			if($this->Customer_model->updateCustomer($id,$customerDBData))
			{
				$this->session->set_flashdata('msg', notification("Service updated successfully!",'success'));
				redirect('customers/edit/'.$id,'location');
			}

		}
		$customerInfo=$this->Customer_model->getCustomer($id);
		$this->data['first_name']=$customerInfo['first_name'];
		$this->data['last_name']=$customerInfo['last_name'];
		$this->data['email']=$customerInfo['email'];
		$this->data['gender']=$customerInfo['gender'];
		$this->data['phone']=$customerInfo['phone'];
		$this->data['address']=$customerInfo['address'];
		$this->template->default_tmpl('customers-entry',$this->data);
	}

	public function delete($id=0)
	{
		$this->Customer_model->updateCustomer($id,array('status'=>'deleted'));
		$this->session->set_flashdata('msg', notification("Customers deleted successfully!",'success'));
		redirect('customers','location');
	}

}

/* End of file Customers.php */
/* Location: ./application/controllers/Customers.php */