<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
	private $data;
	private $limit=10;
	private $offset=0;
	public function __construct()
	{
		parent::__construct();
		$this->load->library("Aauth");
		$this->load->library('form_validation');
		$this->load->model('Users_model');
		$this->load->helper('users');
		if($this->aauth->is_loggedin()===false)
		{
			redirect('login','location');
		}
		if(!$this->aauth->is_admin())
    {
     redirect('sales','location');
    }
		$this->data['userinfo']=$this->session->userdata('userinfo');
		$this->data['search_term']=$this->data['pagination']='';
		$this->data['username']='';
		$this->data['email']='';
		$this->data['password']='';
		$this->data['privilege']='';
		$this->data['confirm_password']='';
		$this->data['privilegeSelected']='';
		$this->data['userGroupData']=array();
		$this->form_validation->set_rules('username', 'Username', 'required|callback_username_check');
    $this->form_validation->set_rules('privilege', 'Privilege', 'required');
    
		$this->form_validation->set_error_delimiters('<div class="alert alert-error alert-dismissible">', '</div>');
		$this->form_validation->set_message('matches', 'New Passwords don\'t match');
		$user_groups=$this->aauth->list_groups();
		$userGroupDataItems['']='Select';
		foreach ($user_groups as $user_group) 
		{
			$userGroupDataItems[$user_group->id]=$user_group->name;
		}
		$this->data['userGroupData']=$userGroupDataItems;
	}

	public function index()
	{
		if($this->input->get('page'))
		{
			$this->page=$this->input->get('page');
			$this->offset = ($this->page  == 1) ? 0 : ($this->page * $this->limit) - $this->limit;
		}
		$results = $this->Users_model->listUsers($this->limit, $this->offset,$this->data['search_term']);
		$this->pagelist= $results['rows'];
		$this->data['pagelist'] = listTable($this->pagelist);
		$this->data['num_results'] = $results['num_rows'];
		$this->load->library('pagination');
		$this->pagnconfig['base_url'] = base_url('users');
		$this->pagnconfig['total_rows'] = $this->data['num_results'];
		$this->pagnconfig['per_page'] = $this->limit;
		$choice = $this->pagnconfig["total_rows"] / $this->pagnconfig["per_page"];
		$this->pagination->initialize($this->pagnconfig);
		$this->data['pagination'] = $this->pagination->create_links();
		$this->template->default_tmpl('users',$this->data);
	}

	public function add()
	{
		$this->form_validation->set_rules('email', 'Email', 'required|is_unique[aauth_users.email]');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|matches[confirm_password]');
    $this->form_validation->set_rules('confirm_password', 'Password Confirmation', 'required|min_length[6]');
		if($this->form_validation->run())
		{
			$username=$this->input->post('username');
			$email=$this->input->post('email');
			$password=$this->input->post('password');
			$confirm_password=$this->input->post('confirm_password');
			$privilege=$this->input->post('privilege');
			$this->data['privilege']=$privilege;
			$this->data['privilegeSelected']=$privilege;
			$userid = $this->aauth->create_user($email, $password, $username);

			if($userid)
			{
				$this->aauth->add_member($userid, $privilege);
				$this->session->set_flashdata('msg', notification("User added successfully!",'success'));
				redirect('users/edit/'.$userid,'location');
			}
		
		}
		$this->template->default_tmpl('user-entry',$this->data);
	}

	public function edit($id)
	{
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('password', 'Password', 'callback_password_check');

		if($id)
		{
			$userinfo=$this->aauth->get_user($id);
			if($userinfo)
			{
				$userGp=$this->aauth->get_user_groups($userinfo->id);
				$privilege='';
				if(isset($userGp['0']))
				{
					$privilege=$userGroupID=$userGp['0']->group_id;
				}
		
				if($this->form_validation->run())
				{
					$username=$this->input->post('username');
					$email=$this->input->post('email');
					$password=$this->input->post('password');
					$confirm_password=$this->input->post('confirm_password');
					$privilege=$this->input->post('privilege');
					$password=($password!='')? $password:FALSE;
					$username=($username!='')? $username:FALSE;
					$this->aauth->update_user($userinfo->id, $email, $password, $username);
					$this->Users_model->deleteUserGroups($userinfo->id);
					$this->aauth->add_member($userinfo->id,$privilege);
					$this->session->set_flashdata('msg', notification("User updated successfully!",'success'));
					redirect('users/edit/'.$userinfo->id,'location');
				}
				$this->data['username']=$userinfo->username;
				$this->data['email']=$userinfo->email;
				$this->data['privilege']=$privilege;
			}
			else
			{
				redirect('users','location');
			}
			
		}
		else
		{
			
		}
		$this->template->default_tmpl('user-entry',$this->data);
	}

	public function username_check($str)
  {
  	$userinfo=$this->Users_model->getUser($str);
    if ($userinfo)
    {
      $this->form_validation->set_message('username_check', 'The {field} field already exits');
      return FALSE;
    }
    else
    {
      return TRUE;
    }
  }
  public function password_check($password='',$confirm_password='')
  {
    if ($password!='' && ($password!=$this->input->post('confirm_password')) )
    {
      $this->form_validation->set_message('password_check', 'The confirm {field} mismatch');
      return FALSE;
    }
    else
    {
      return TRUE;
    }
  }
}

/* End of file Users.php */
/* Location: ./application/controllers/Users.php */