<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <!--        <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?php design_images('avatar.png'); ?>" class="img-circle" alt="User Image">
                    </div>
                    <div class="pull-left info">
                        <p><?php echo ucwords($userinfo->username); ?></p>
                         <a href="#"><i class="fa fa-circle text-success"></i> Online</a> 
                    </div>
                </div>-->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <?php if ($is_admin) { ?>
                <li>
                    <a href="<?php echo base_url('dashboard'); ?>">
                        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                    </a>
                </li>
            <?php } ?>
            <?php if (!$is_admin) { ?>
                <li>
                    <a href="<?php echo base_url('sales'); ?>">
                        <i class="fa fa-money"></i> <span>POS</span>
                    </a>
                </li>
            <?php } ?>
            <?php if ($is_admin) { ?>
                <li class=" treeview">
                    <a href="<?php echo base_url('services'); ?>">
                        <i class="fa fa-cutlery"></i> <span>Items</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="<?php echo base_url('services'); ?>"><i class="fa fa-circle-o"></i> List</a></li>
                        <li><a href="<?php echo base_url('services/add'); ?>"><i class="fa fa-circle-o"></i> Add new</a></li>
                    </ul>
                </li>
                <li class=" treeview">
                    <a href="<?php echo base_url('categories'); ?>">
                        <i class="fa fa-list-ul"></i> <span>Categories</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="<?php echo base_url('categories'); ?>"><i class="fa fa-circle-o"></i> List</a></li>
                        <li><a href="<?php echo base_url('categories/add'); ?>"><i class="fa fa-circle-o"></i> Add new</a></li>
                    </ul>
                </li>
            <?php } ?>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-user"></i> <span>Customers</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="active"><a href="<?php echo base_url('customers'); ?>"><i class="fa fa-circle-o"></i> List</a></li>
                    <li><a href="<?php echo base_url('customers/add'); ?>"><i class="fa fa-circle-o"></i> Add new</a></li>
                </ul>
            </li>
            <?php if ($is_admin) { ?>
                <li class="treeview">
                    <a href="<?php echo base_url('service-person'); ?>">
                        <i class="fa fa-user"></i> <span><?php echo $this->config->item('app_service_person'); ?>s</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="<?php echo base_url('service-person'); ?>"><i class="fa fa-circle-o"></i> List</a></li>
                        <li><a href="<?php echo base_url('service-person/add'); ?>"><i class="fa fa-circle-o"></i> Add new</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-briefcase"></i> <span>Inventory</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="<?php echo base_url('inventory'); ?>"><i class="fa fa-circle-o"></i> List</a></li>
                        <li class="active"><a href="<?php echo base_url('inventory/add'); ?>"><i class="fa fa-circle-o"></i> Re-Stock</a></li>
                        <li class="active"><a href="<?php echo base_url('inventory/addUsed'); ?>"><i class="fa fa-circle-o"></i> Record Used Items</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-bar-chart"></i> <span>Reports</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="<?php echo base_url('sales-report'); ?>"><i class="fa fa-circle-o"></i> Sales</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-users"></i> <span>Users</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="<?php echo base_url('users'); ?>"><i class="fa fa-circle-o"></i> List</a></li>
                        <li class="active"><a href="<?php echo base_url('users/add'); ?>"><i class="fa fa-circle-o"></i> Add</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-wrench"></i> <span>Tools</span>
                        <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                        </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="active"><a href="<?php echo base_url('update'); ?>"><i class="fa fa-cloud-download"></i> Update Software</a></li>
    <!--                        <li class="active"><a href="<?php echo base_url('users/add'); ?>"><i class="fa fa-circle-o"></i> Add</a></li>-->
                    </ul>
                </li>
            <?php } ?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
