<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Sales Report
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <div class="col-xs-12">

                <div class="box">
                    <div class="box-body table-responsive">
                        <div class="box-tools ">
                            <form method="get">

                                <div class="col-xs-4 pull-right">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        <input type="text" class="form-control pull-right" name="salesdaterange" id="salesdaterange">
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-info btn-flat">Apply</button>
                                        </span>
                                    </div>
                                </div>

                                <div class="col-xs-2 pull-right">
                                    <div class="form-group">
                                        <?php echo form_dropdown('category_id', $categories, $selectedCategory, array('class' => 'form-control', 'id' => 'category', 'tabindex' => '1')); ?>
                                    </div>
                                </div>
                                <div class="col-xs-2 pull-right">
                                    <div class="form-group">
                                        <?php echo form_dropdown('service', $servicedata, $serviceSelected, array('class' => 'form-control', 'id' => 'service', 'tabindex' => '1')); ?>
                                    </div>
                                </div>
                                <div class="col-xs-2 pull-right">
                                    <div class="form-group">
                                        <?php echo form_dropdown('customer', $customerdata, $customerSelected, array('class' => 'form-control', 'id' => 'customer', 'tabindex' => '1')); ?>
                                    </div>
                                </div>
                                <div class="col-xs-2 pull-right">
                                    <div class="form-group">
                                        <?php echo form_dropdown('serviceperson', $servicePersonData, $servicepersonSelected, array('class' => 'form-control', 'id' => 'serviceperson', 'tabindex' => '1')); ?>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-xs-12">
                            <?php echo $reportlist; ?>
                        </div>  
                    </div>

                </div>


            </div>
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->