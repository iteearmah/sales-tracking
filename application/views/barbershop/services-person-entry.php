  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Service Person
        <?php $page_mode=($this->uri->segment(2)!='edit')? 'new':'edit';?>
        <small><?php echo ucwords($page_mode);?></small>
      </h1>
      <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
        
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
       <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Service Entry</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post">
            <?php echo $this->session->flashdata('msg');?>
              <div class="box-body">
                <div class="form-group">
                  <label for="inputname">Name</label>
                  <input type="text" class="form-control" name="name" value="<?php echo set_value('name',$name); ?>" id="inputname" tabindex="1" placeholder="<?php echo $this->config->item('app_service_person');?> name">
                </div>
                <?php echo form_error('name'); ?>
                 <div class="form-group">
                  <label for="inputname">Phone</label>
                  <input type="tel" class="form-control" name="phone" value="<?php echo set_value('phone',$phone); ?>" id="inputphone" tabindex="2" placeholder="<?php echo $this->config->item('app_service_person');?> phone number">
                </div>
               <div class="form-group">
                  <label for="inputname">Started Work</label>
                  <input type="date" class="form-control" name="started_work" value="<?php echo set_value('started_work',$started_work); ?>" id="inputstarted_work" tabindex="3" placeholder="Started work when?">
                </div>
                <div class="form-group">
                  <label for="inputname">Terminated Work</label>
                  <input type="date" class="form-control" name="terminated_work" value="<?php echo set_value('terminated_work',$terminated_work); ?>" id="inputstarted_work" tabindex="4" placeholder="Terminated work when?">
                </div>
                <div class="form-group">
                  <label>Address</label>
                  <textarea class="form-control" tabindex="5" rows="3" name="address" placeholder="<?php echo $this->config->item('app_service_person');?> Address"><?php echo set_value('address',$address); ?></textarea>
                </div>
                <div class="form-group">
                  <label>Status</label>
                    <?php echo form_dropdown('status', $statusdata, $status,array('class'=>'form-control')); ?>
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary btn-flat pull-right">Submit</button>
              </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->