  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User
        <?php $page_mode=($this->uri->segment(2)!='edit')? 'new':'edit';?>
        <small><?php echo ucwords($page_mode);?></small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <div class="col-xs-8">
        <?php echo $this->session->flashdata('msg');?>
        <form method="post">
          <div class="box">
            <div class="box-body">
              <div class="form-group">
                <div class="infobox"></div>
                <label for="inputname">Username</label>
                <input class="form-control input" tabindex="1" id="username" name="username" type="text" placeholder="Username (required)" value="<?php echo set_value('username',$username); ?>">
                <?php echo form_error('username'); ?>
              </div>
               <div class="form-group">
              <div class="infobox"></div>
              <label for="inputname">Email</label>
              <input class="form-control input" tabindex="2" id="email" name="email" type="email" placeholder="Email (required)" value="<?php echo set_value('email',$email); ?>">
              <?php echo form_error('email'); ?>
            </div>

            <div class="form-group">
              <div class="infobox"></div>
              <label for="inputname">Password</label>
              <input class="form-control input" tabindex="3" id="password" name="password" type="password" placeholder="Password (required)" value="">
              <?php echo form_error('password'); ?>
            </div>

            <div class="form-group">
              <div class="infobox"></div>
              <label for="inputname">Confirm Password</label>
              <input class="form-control input" tabindex="4" id="confirm_password" name="confirm_password" type="password" placeholder="Password (required)" value="">
              <?php echo form_error('confirm_password'); ?>
              <?php echo form_error('matches'); ?>
            </div>
            <div class="form-group">
                  <label>Privilege</label>
                    <?php echo form_dropdown('privilege', $userGroupData, $privilege,array('class'=>'form-control','id'=>'privilege','tabindex'=>'5')); ?>
                    <?php echo form_error('privilege'); ?>
                </div>

            <button type="submit" tabindex="6" class="btn btn-block btn-lg btn-primary btn-flat" id="submit" name="submit" >Submit</button>
            </div>
          </div>
          </form>
        </div>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->