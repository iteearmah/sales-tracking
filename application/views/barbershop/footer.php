
<!-- /.control-sidebar -->
<!-- Add the sidebar's background. This div must be placed
     immediately after the control sidebar -->
<div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="<?php design_js('jquery-2.2.3.min.js'); ?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php design_js('jquery-ui.min.js'); ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php design_js('bootstrap.min.js'); ?>"></script>
<script src="<?php design_js('pace.min.js'); ?>"></script>
<script src="<?php design_js('app.js'); ?>"></script>
<script src="<?php design_plugins('daterangepicker/moment.min.js'); ?>"></script>
<script src="<?php design_plugins('daterangepicker/daterangepicker.js'); ?>"></script>
<script type="text/javascript">

    $(document).ready(function () {
        $('#salesdaterange').daterangepicker({
            "startDate": "<?php echo (isset($start_date)) ? gmdate('m/d/Y', strtotime($start_date)) : gmdate('m/d/Y'); ?>",
            "endDate": "<?php echo (isset($end_date)) ? gmdate('m/d/Y', strtotime($end_date)) : gmdate('m/d/Y'); ?>",
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });
    });
</script>
<script src="<?php design_js('angularjs/angular.min.js') ?>"></script>
<script src="<?php design_js('angularjs/angular-route.min.js') ?>"></script>
<script src="<?php design_js('angularjs/angular-resource.min.js') ?>"></script>
<script src="<?php design_js('angularjs/ui-bootstrap.min.js') ?>"></script>
<script src="<?php design_js('angularjs/app.js') ?>"></script>
</body>
</html>
