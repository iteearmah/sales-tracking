<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <?php echo $this->session->flashdata('msg'); ?>

        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <?php
            echo numSales();
            echo numCustomers();
            echo numServices();
            echo numServicePersons();
            ?>
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->