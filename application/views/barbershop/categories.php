  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Menu Categories
        <small>List</small>
      </h1>
     <!--  <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <div class="col-xs-12">
        <?php echo $this->session->flashdata('msg');?>
         <div class="box">
          <div class="box-body table-responsive">
              <table class="table table-striped">
                  <tbody>
                      <tr>
                          <th>Name</th>
                          <th>Description</th>
                          <th></th>
                      </tr>
                      <?php foreach($pagelist as $item):?>
                      <tr>
                          <td><a href="<?php echo base_url('categories/edit/'.$item['id']) ?>"><?php echo $item['name']?></a></td>
                          <td><?php echo $item['description']?></td>
                          <td><a href="<?php echo base_url('categories/edit/'.$item['id'])?>" class="btn btn-default btn-flat" title="Edit"><i class="fa fa-edit"></i></a>  
                              <a href="<?php echo base_url('categories/delete/'.$item['id'])?>" class="btn  btn-danger btn-flat" title="Delete"><i class="fa fa-trash"></i></a></td>
                      </tr>
                      <?php endforeach; ?>
                  </tbody>
              </table>
          <div class="box-tools pull-right"><?php echo $pagination;?></div>
          </div>
          </div>
        </div>
    </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->