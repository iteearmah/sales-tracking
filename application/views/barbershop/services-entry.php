<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Item
            <?php $page_mode = ($this->uri->segment(2) != 'edit') ? 'new' : 'edit'; ?>
            <small><?php echo ucwords($page_mode); ?></small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Item Entry</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="post">
                        <?php echo $this->session->flashdata('msg'); ?>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputname" class="control-label">Name</label>
                                <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $serviceInfo['service_name']); ?>" id="inputname" tabindex="1" placeholder="Item name" required>
                                <?php echo form_error('name'); ?>
                            </div>

                            <div class="form-group">
                                <label for="inputname" class="control-label">Unit Price</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><?php echo $this->config->item('app_currency'); ?></span>
                                    <input type="number" class="form-control" name="price" value="<?php echo set_value('price', $serviceInfo['service_price']); ?>" tabindex="2" placeholder="Service price e.g(5.00)">
                                    <?php echo form_error('price'); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Category</label>
                                <?php echo form_dropdown('category_id', $typedata, $serviceInfo['category_id'], array('class' => 'form-control', 'tabindex' => 3)); ?>
                                <?php echo form_error('category_id'); ?>
                            </div>

                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="inventory_part" <?php echo set_value('inventory_part', $serviceInfo['inventory_part'] ? 'checked' : ''); ?> tabindex="4"> Add to Inventory
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="not_for_sale" <?php echo set_value('for_sale', $serviceInfo['not_for_sale'] ? 'checked' : ''); ?> tabindex="5"> This item is not for sale
                                    </label>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-flat btn-primary pull-right">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->