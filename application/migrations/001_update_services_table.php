<?php

/**
 * This migration updates services table so it can work
 * for food items.
 * 
 * @property CI_DB_forge $dbforge 
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Update_services_table extends CI_Migration {

    public function up() {
        $this->dbforge->add_column('services', array(

            'category_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE
            )
            , 'inventory_part' => array(
                'type' => 'TINYINT',
                'constraint' => '1',
                'unsigned' => TRUE,
                'default' => 0
            )
        ));
    }

    public function down() {
        $this->dbforge->drop_column('services', array('min_price', 'step_price'));
    }

}
