<?php

/**
 * This migration updates the stock table.
 * I have added 'first' to the name so that we can differentiate other updates to same table
 * 
 * @property CI_DB_forge $dbforge 
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Second_update_services_table extends CI_Migration {

    public function up() {
        $this->dbforge->add_column('services', array(
            'not_for_sale' => array(
                'type' => 'TINYINT',
                'constraint' => '1',
                'unsigned' => TRUE,
                'default' => 0
            )
        ));
    }

    public function down() {
        $this->dbforge->drop_column('services', 'not_for_sale');
    }

}
